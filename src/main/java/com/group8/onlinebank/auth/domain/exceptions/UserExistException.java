package com.group8.onlinebank.auth.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserExistException extends Exception {
    public UserExistException(String message) {
        super(message);
    }
}
