package com.group8.onlinebank.auth.infra.dto;

import com.group8.onlinebank.account.domain.Account;
import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import lombok.Data;

import java.util.UUID;

@Data
public class RegisterResponseDto {
    private UUID id;
    private String message;
    private Account account;

    public RegisterResponseDto(UUID id, String message, Account account) {
        this.id = id;
        this.message = message;
        this.account = account;
    }
}
