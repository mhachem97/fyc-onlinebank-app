package com.group8.onlinebank.auth.infra.repository;


import com.group8.onlinebank.auth.domain.Role;
import com.group8.onlinebank.auth.domain.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleEnum name);
}
