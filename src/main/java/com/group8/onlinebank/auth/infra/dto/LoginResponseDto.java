package com.group8.onlinebank.auth.infra.dto;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Optional;
import java.util.UUID;

@Data
public class LoginResponseDto {
    private String token;
    private UUID id;
    private String firstname;
    private String lastname;
    private String phone;
    private String email;
    private Optional<? extends GrantedAuthority> role;

    public LoginResponseDto(
            String accessToken,
            UUID id,
            String firstname,
            String lastname,
            String phone,
            String email,
            Optional<? extends GrantedAuthority> role) {
        this.token = accessToken;
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;

    }
}
