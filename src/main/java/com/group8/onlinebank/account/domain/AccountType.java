package com.group8.onlinebank.account.domain;

public enum AccountType {
    ACCOUNT_TYPE_COURANT,
    ACCOUNT_TYPE_EPARGNE,
}
