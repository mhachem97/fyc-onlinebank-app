package com.group8.onlinebank.account.domain;

public enum AccountStatus {
    ACCOUNT_STATUS_ACTIVATED,
    ACCOUNT_STATUS_PENDING,
    ACCOUNT_STATUS_BLOCKED,
    ACCOUNT_STATUS_ABORTED

}