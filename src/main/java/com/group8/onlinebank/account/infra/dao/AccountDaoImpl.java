package com.group8.onlinebank.account.infra.dao;

import com.group8.onlinebank.account.domain.Account;
import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.account.infra.mapper.AccountMapper;
import com.group8.onlinebank.account.infra.repository.AccountRepository;
import com.group8.onlinebank.client.infrastructure.controller.CustomerController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@Slf4j
@RequiredArgsConstructor
public class AccountDaoImpl implements AccountDao {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountEntity store(Account account) {
        return accountRepository.save(accountMapper.map(account));
    }

    @Override
    public List<AccountEntity> getAllUserAccount(String customerId) {
        Logger logger = LogManager.getLogger(CustomerController.class);
        logger.info(customerId);
        logger.info(accountRepository.findByUserId(customerId));
        return accountRepository.findByUserId(customerId);
    }

    @Override
    public AccountEntity getUserAccountByAccountNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public void updateUserAccountBalance(String accountNumber, double accountBalance) {
        AccountEntity userAccount = getUserAccountByAccountNumber(accountNumber);

        userAccount.setAccountBalance(accountBalance);
        accountRepository.save(userAccount);
    }
}
