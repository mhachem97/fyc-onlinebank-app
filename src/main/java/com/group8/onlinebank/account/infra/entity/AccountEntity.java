package com.group8.onlinebank.account.infra.entity;

import com.group8.onlinebank.account.domain.AccountStatus;
import com.group8.onlinebank.account.domain.AccountType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "account")
@Accessors(chain = true)
public class AccountEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column(name = "user_id")
    private String userId;
    @NotBlank
    @Column(name = "account_number")
    private String accountNumber = "OB-" + UUID.randomUUID().toString();
    @Column(name = "account_type")
    private AccountType accountType;
    @Column(name = "account_balance")
    private double accountBalance = 0.0;
    @Column(name = "account_status")
    private AccountStatus accountStatus = AccountStatus.ACCOUNT_STATUS_PENDING;
    @CreationTimestamp
    private LocalDateTime created_at;
    @UpdateTimestamp
    private LocalDateTime updated_at;

}
