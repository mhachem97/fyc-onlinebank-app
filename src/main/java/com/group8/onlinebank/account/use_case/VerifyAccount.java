package com.group8.onlinebank.account.use_case;

import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VerifyAccount {

    private final AccountDao accountDao;

    public boolean execute(String accountNumber) {

        AccountEntity accountEntity = accountDao.getUserAccountByAccountNumber(accountNumber);

        if (accountEntity == null) {
            return false;
        }
        return true;
    }
}
