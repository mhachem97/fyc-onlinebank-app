package com.group8.onlinebank.account.use_case;

import com.group8.onlinebank.account.domain.Account;
import com.group8.onlinebank.account.domain.AccountBuilder;
import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.domain.AccountStatus;
import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.infra.dto.AccountRequestDto;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.account.infra.mapper.AccountMapper;
import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.transaction.domain.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CreateAccount {

    private final AccountDao accountDao;
    private final UserDAO userDAO;

    public AccountEntity execute(AccountRequestDto accountRequestDto) throws UserNotExistException {

        UUID userId = UUID.fromString(accountRequestDto.getUserId());
        boolean userExist = userDAO.existByUserId(userId);
        if(!userExist) {
            throw new UserNotExistException("This user not found");
        }

        Account account = new AccountBuilder()
                .setAccountType(accountRequestDto.getAccountType())
                .setAccountStatus(AccountStatus.ACCOUNT_STATUS_PENDING)
                .setAccountBalance(200)
                .setUserId(accountRequestDto.getUserId())
                .createAccount();

        return accountDao.store(account);

    }
}
