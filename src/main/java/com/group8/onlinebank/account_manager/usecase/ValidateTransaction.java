package com.group8.onlinebank.account_manager.usecase;
import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.account.use_case.UpdateBalance;
import com.group8.onlinebank.client.domain.Client;
import com.group8.onlinebank.client.domain.exceptions.InsufficientFundsException;
import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.domain.TransactionDao;
import com.group8.onlinebank.transaction.domain.TransactionService;
import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.exceptions.InvalidTypeException;
import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import com.group8.onlinebank.transaction.infra.dto.TransactionResponse;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import com.group8.onlinebank.transaction.infra.mapper.TransactionMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class ValidateTransaction {

    private final TransactionService transactionService;
    private final Client client;

    private final UpdateBalance updateBalance;
    private final TransactionDao transactionDao;
    private final TransactionMapper transactionMapper;



    public TransactionResponse execute(String transactionId) throws InvalidTypeException, InsufficientFundsException, AccountNotExistException {
        UUID UUIDTRansactionId = UUID.fromString(transactionId);
        Optional<TransactionEntity> optionalTransactionEntity=  transactionDao.getTransactionById(UUIDTRansactionId);
        TransactionEntity transactionEntity = null ;
        if(optionalTransactionEntity.isPresent()) {
            transactionEntity = optionalTransactionEntity.get();
            transactionEntity.setStatus((TransactionStatus.TRANSACTION_STATUS_APPROVED));

            transactionEntity = transactionDao.updateTransaction(transactionMapper.map(transactionEntity));

            double accountBalance = transactionService.getBalance(transactionEntity.getAccountNumber());
            updateBalance.execute(transactionEntity.getAccountNumber(), accountBalance);
        }

        return new TransactionResponse(transactionEntity);
    }


}
