package com.group8.onlinebank.account_manager.infrastrucuture;

import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.account.use_case.GetCustomerAccount;
import com.group8.onlinebank.account_manager.usecase.GetPendingTransactions;
import com.group8.onlinebank.account_manager.usecase.ValidateAccount;
import com.group8.onlinebank.account_manager.usecase.ValidateTransaction;
import com.group8.onlinebank.client.domain.exceptions.InsufficientFundsException;
import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.exceptions.InvalidTypeException;
import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/public/v0/customerManager")
public class AccountManagerController {
    ValidateTransaction validateTransaction;
    ValidateAccount validateAccount;
    GetPendingTransactions getPendingTransactions;


    @PutMapping("/validateTransaction/{transactionId}")
    public ResponseEntity<?> validateTransaction(@PathVariable String transactionId) throws InvalidTypeException, AccountNotExistException, InsufficientFundsException {

        return ResponseEntity.ok(validateTransaction.execute(transactionId));
    }

    @PutMapping("/validateAccount")
    public  ResponseEntity<?> validateAccount(String accountNumber) throws AccountNotExistException {
        return ResponseEntity.ok(validateAccount.execute(accountNumber));
    }


    @GetMapping("/{accountNumber}")
    public ResponseEntity<?> getPendingTransactions(@PathVariable  String accountNumber) throws AccountNotExistException {
        return ResponseEntity.ok(getPendingTransactions.execute(accountNumber));
    }






}
