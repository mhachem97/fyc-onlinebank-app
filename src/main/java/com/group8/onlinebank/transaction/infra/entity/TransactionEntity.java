package com.group8.onlinebank.transaction.infra.entity;

import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.TransactionType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Entity
@Data
@Table(name = "transaction")
@Accessors(chain = true)
public class TransactionEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column(name = "user_id")
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID userId;
    @Column(name = "account_number")
    private String accountNumber;
    @Column(name = "recipient_account_number")
    private String recipientAccountNumber = null;
    @Column
    private double amount;
    @Column
    private TransactionType type;
    @Column
    private TransactionStatus status;
    @CreationTimestamp
    private LocalDateTime created_at;
    @UpdateTimestamp
    private LocalDateTime updated_at;

}

