package com.group8.onlinebank.transaction.infra.repository;

import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<TransactionEntity, UUID> {
    List<TransactionEntity> findByAccountNumber(String account_number);
    List<TransactionEntity> findByUserId(UUID userId);
}
