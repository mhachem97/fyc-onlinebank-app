package com.group8.onlinebank.transaction.infra.dto;

import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.TransactionType;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransactionResponse {

    private UUID id;
    private UUID userId;
    private String accountNumber;
    private String recipientAccountNumber;
    private double amount;
    private TransactionType type;
    private TransactionStatus status;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;

    public TransactionResponse(TransactionEntity transactionEntity) {
        this.id = transactionEntity.getId();
        this.userId = transactionEntity.getUserId();
        this.accountNumber = transactionEntity.getAccountNumber();
        this.amount = transactionEntity.getAmount();
        this.type = transactionEntity.getType();
        this.status = transactionEntity.getStatus();
        this.created_at = transactionEntity.getCreated_at();
        this.updated_at = transactionEntity.getUpdated_at();
    }
}
