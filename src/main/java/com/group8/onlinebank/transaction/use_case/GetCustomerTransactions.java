package com.group8.onlinebank.transaction.use_case;

import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.domain.TransactionDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GetCustomerTransactions {

    private final TransactionDao transactionDao;
    private final UserDAO userDAO;

    public List<Transaction> execute(String userId) throws UserNotExistException {
        UUID userIdUUID = UUID.fromString(userId);

        boolean userExist = userDAO.existByUserId(userIdUUID);
        if(!userExist) {
            throw new UserNotExistException("This user not found");
        }

        return transactionDao.getAllByUserId(userIdUUID);
    }
}
