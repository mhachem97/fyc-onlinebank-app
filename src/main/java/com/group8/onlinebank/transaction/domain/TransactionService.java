package com.group8.onlinebank.transaction.domain;

import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import com.group8.onlinebank.transaction.infra.mapper.TransactionMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionEntity transactionEntity;
    private final TransactionMapper transactionMapper;
    private final TransactionDao transactionDao;

    public TransactionEntity addTransaction(TransactionRequest transactionRequest) {
        UUID userId = UUID.fromString(transactionRequest.getUserId());
        transactionEntity.setUserId(userId);
        transactionEntity.setAccountNumber(transactionRequest.getAccountNumber());
        if (transactionRequest.getType() == TransactionType.TRANSACTION_TYPE_TRANSFER) {
            transactionEntity.setRecipientAccountNumber(transactionRequest.getRecipientAccountNumber());
        }
        transactionEntity.setAmount(transactionRequest.getAmount());
        transactionEntity.setType(transactionRequest.getType());
        transactionEntity.setStatus(transactionRequest.getStatus());

        return transactionDao.store(transactionMapper.map(transactionEntity));
    }

    public double getBalance(String account_number) {
        List<Transaction> transactions = transactionDao.getAllByAccountNumber(account_number);
        AtomicReference<Double> balance = new AtomicReference(Double.valueOf(0.0));

        transactions.forEach(t -> {
            if(t.getStatus() == TransactionStatus.TRANSACTION_STATUS_APPROVED) {
                if (t.getType() == TransactionType.TRANSACTION_TYPE_PAYMENT) {
                    balance.set(balance.get() + t.getAmount());
                } else {
                    balance.set(balance.get() - t.getAmount());
                }
            }
        });

        return balance.get();
    }

    public List<Transaction> filterPendingTransactions(List<Transaction> transactions){
        return transactions.stream().filter(
                transaction -> transaction.getStatus() == TransactionStatus.TRANSACTION_STATUS_PENDING)
                .collect(Collectors.toList());

    }

    public List<Transaction> filterTransactionsUntilDate(List<Transaction> transactions, LocalDateTime date) {
        return transactions.stream().filter(
                transaction -> transaction.getCreated_at().isBefore(date))
                .collect(Collectors.toList());

    }

    public double getBalance(String accountNumber, String date) {
        List<Transaction> transactions = transactionDao.getAllByAccountNumber(accountNumber);
        LocalDateTime limitDate = convertDateStringToLocalDateTime(date);
        List<Transaction> filteredTransactions = filterTransactionsUntilDate(transactions, limitDate);
        AtomicReference<Double> balance = new AtomicReference(Double.valueOf(0.0));

        filteredTransactions.forEach(t -> {
            if(t.getStatus() == TransactionStatus.TRANSACTION_STATUS_APPROVED) {
                if (t.getType() == TransactionType.TRANSACTION_TYPE_PAYMENT) {
                    balance.set(balance.get() + t.getAmount());
                } else {
                    balance.set(balance.get() - t.getAmount());
                }
            }
        });

        return balance.get();
    }

    public LocalDateTime convertDateStringToLocalDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(date + " 23:59", formatter);
    }
}
