package com.group8.onlinebank.transaction.domain;

public class TransactionThreshold {

    public static final double PAYMENT_THRESHOLD = 2000;
    public static final double WITHDRAWAL_THRESHOLD = 1000;
    public static final double TRANSFER_THRESHOLD = 5000;

}
