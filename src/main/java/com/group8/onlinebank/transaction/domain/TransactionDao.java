package com.group8.onlinebank.transaction.domain;

import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionDao {

    TransactionEntity store(Transaction transaction);
    Optional<TransactionEntity> getTransactionById(UUID transactionId);
    List<Transaction> getAllByAccountNumber(String accountNumber);
    List<Transaction> getAllByUserId(UUID userId);
    TransactionEntity updateTransaction(Transaction transaction);
}
