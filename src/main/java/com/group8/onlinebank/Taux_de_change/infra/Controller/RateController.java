package com.group8.onlinebank.Taux_de_change.infra.Controller;
import com.group8.onlinebank.Taux_de_change.use_case.GetCurrencyRates;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/public/v0/rates")
public class RateController {

    private final GetCurrencyRates getCurrencyRates;


        @GetMapping
        public ResponseEntity<?> rates() {
            return ResponseEntity.ok(getCurrencyRates.execute());
        }


    }





